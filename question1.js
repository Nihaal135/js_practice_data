/*
Q.1 Get all addresses from userDetailsArray using both Map & Reduce and return the result in an array.

Response : 
[{
            streetAddress: '48 Flatlands Avenue',
            neighbour: 'Cutter',
            city: 'North Dakota'
        }, {
            streetAddress: '77 Hemlock Street',
            neighbour: 'Hasty',
            city: 'Florida'
}]*/

let values = require('./inventory');

const resultReduce = values.reduce((gatherer,current)=>{
    gatherer.push(current.address)
    return gatherer;
},[]);

console.log(resultReduce);

const resultUsingMap = values.map((value) => value.address)

console.log(resultUsingMap)