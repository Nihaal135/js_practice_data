// Q.3 Get all user details in the following object format using reduce.

// Response:
// {
//         600dc3b5d617e547a0e74cb9": {
//             "name": "Mitchell Fitzgerald",
//            s": "48 Flatlands Avenue, Cutter, North Dakota",
//             address: "about": "Proident voluptate veniam voluptate mollit reprehenderit anim officia et ea ex laboris nulla laboris. Nulla ut aliquip fugiat tempor veniam sint aliqua reprehenderit tempor Lorem commodo anim.",
//             "addres {
//                 streetAddress: '48 Flatlands Avenue',
//                 neighbour: 'Cutter',
//                 city: 'North Dakota'
//             },
//             "company": "Scenty"
//         },
//         600dc3b5c4e60ba2ebf06569: {
//         "name": "Howell Faulkner",
//         "about": "Mollit Lorem reprehenderit qui elit id aliqua. Deserunt ipsum ad cupidatat ullamco ut aliqua est do consectetur nostrud sit esse.",
//         address: {
//             streetAddress: '77 Hemlock Street',
//             neighbour: 'Hasty',
//             city: 'Florida'
//         },
//         "company": "Fleetmix"
//     }
// }

let values = require('./inventory');

let result = values.reduce((gatherer,current)=>{
    let newId = current['id'];
    gatherer[newId] = {...current,address:{...current.address}};
    delete gatherer[newId][newId];
    return gatherer;
},{});

console.log(result);