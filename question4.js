// Q4. Filter out the person who lives in city "North Dakota" using reduce and generate the following result

// Response:
// {
//       600dc3b5d617e547a0e74cb9": {
//             information: ["Mitchell Fitzgerald", "North Dakota", "Scenty"]
//       }
// }
let values = require('./inventory');

let result = values.reduce((gatherer,current)=>{
    if(current['address']['city'] === "North Dakota"){
        gatherer[current['id']] = {'information' : [current['name'],current['address']['city'], current['company']]};
    }
    return gatherer;
},{});

console.log(result);