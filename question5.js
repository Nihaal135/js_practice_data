// Q5. Filter out the person who lives in city "North Dakota" and transform the result to the following structure. 
// Response: 
// [{id: 600dc3b5d617e547a0e74cb9, name: Mitchell Fitzgerald"}, "North Dakota", "Scenty"]
let values = require('./inventory');

let result = values.reduce((gatherer,current)=>{
    if(current['address']['city'] === "North Dakota"){
        gatherer.push({'id' : current['id'], 'name' : current['name']}, current['address']['city'], current['company']);
    }
    return gatherer;
},[]);

console.log(result);