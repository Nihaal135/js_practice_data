// Q7. Transform into following Object
// Response:
// [
//     [{id: 600dc3b5d617e547a0e74cb9, name: Mitchell Fitzgerald"}, { city: "North Dakota" }, { company: "Scenty" }],
//     [{id: 600dc3b5c4e60ba2ebf06569, name: Howell Faulkner"}, { city: "Florida" }, { company: "Fleetmix" }]
// ]

let values = require('./inventory');

let result = values.reduce((gatherer, current)=>{
    let details = [{'id': current['id'], 'name': current['name']}, { 'city': current['address']['city']}, { 'company': current['company']}]
    gatherer.push(details);
    return gatherer;
},[]);

console.log(result);